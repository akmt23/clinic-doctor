import React, { useState, useEffect } from "react";
import "chart.js/auto";
import { Chart } from "react-chartjs-2";

const MONTHS = [
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря",
];

interface Props {
    stats: any;
}

const BarChart = ({ stats }: Props) => {
    const [data, setData] = useState({
        labels: [],
        datasets: [
            {
                label: "Прибыль",
                data: [],
                backgroundColor: "rgba(255, 99, 132, 0.2)",
                borderColor: "rgba(255, 99, 132, 1)",
                borderWidth: 1,
                fill: true,
                tension: 0.3,
            },
        ],
    });

    useEffect(() => {
        const chartLabel = stats.data.filter(
            (item: any) => item.type == "Money",
        );

        const labels = chartLabel.map(
            (item: any) => `${item.day}  ${MONTHS[item.month - 1]}`,
        );

        const values = chartLabel.map((item: any) => `${item.sum}`);

        setData({
            labels: labels,
            datasets: [
                {
                    label: "Прибыль",
                    data: values,
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    borderColor: "rgba(255, 99, 132, 1)",
                    borderWidth: 1,
                    fill: true,
                    tension: 0.3,
                },
            ],
        });
    }, [stats]);

    return (
        <div className="text-black">
            <Chart
                type="line"
                data={data}
                height={400}
                width={600}
                options={{
                    maintainAspectRatio: false,
                }}
            />
        </div>
    );
};

export default BarChart;
