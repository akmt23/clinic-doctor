import React, { useState, useEffect } from "react";
import "chart.js/auto";
import { Chart } from "react-chartjs-2";

const MONTHS = [
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря",
];

interface Props {
    stats: any;
}

const BarChart = ({ stats }: Props) => {
    const [data, setData] = useState({
        labels: [],
        datasets: [
            {
                label: "Кол-во пациентов",
                data: [],
                backgroundColor: "rgba(255, 99, 132, 0.2)",
                borderColor: "#58d432",
                borderWidth: 1,
                fill: true,
                tension: 0.3,
            },
        ],
    });

    useEffect(() => {
        const chartLabel = stats.data.filter(
            (item: any) => item.type == "people",
        );

        const labels = chartLabel.map(
            (item: any) => `${item.day}  ${MONTHS[item.month - 1]}`,
        );

        const values = chartLabel.map((item: any) => `${item.sum}`);

        setData({
            labels: labels,
            datasets: [
                {
                    label: "Кол-во пациентов",
                    data: values,
                    backgroundColor: "rgba(98, 201, 84, 0.2)",
                    borderColor: "#58d432",
                    borderWidth: 1,
                    fill: true,
                    tension: 0.3,
                },
            ],
        });
    }, [stats]);
    return (
        <div className="text-black">
            <Chart
                type="line"
                data={data}
                height={400}
                width={600}
                options={{
                    maintainAspectRatio: false,
                }}
            />
        </div>
    );
};

export default BarChart;
