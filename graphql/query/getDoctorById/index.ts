import { gql } from "@apollo/client";

export const GET_DOCTOR_BY_ID = gql`
    query ($doctorId: String!) {
        getDoctorByID(doctorId: $doctorId) {
            _id
            fullName
            dateOfBirth
            acceptableAgeGroup
            description
            email
            fullName
            numOfRatings
            phoneNumber
            rating
            timelines {
                _id
                startDate
                endDate
                booking {
                    _id
                    startDate
                    endDate
                    service {
                        _id
                        price
                        description
                    }
                }
            }
        }
    }
`;
