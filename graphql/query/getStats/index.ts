import { gql } from "@apollo/client";

export const GET_STATS = gql`
    query (
        $firstDate: DateTime!
        $secondDate: DateTime!
        $period: AllowedPeriodsOfTime!
    ) {
        getGeneralStats(
            firstDate: $firstDate
            secondDate: $secondDate
            period: $period
        ) {
            data {
                sum
                month
                year
                day
                type
            }
            totalIndividualPatients
            totalMoneyEarnt
            totalSessionSum
            startDate
            endDate
        }
    }
`;
