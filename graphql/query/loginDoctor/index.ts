import { gql } from "@apollo/client";

export const LOGIN_DOCTOR = gql`
    query ($email: String!, $password: String!) {
        loginDoctor(email: $email, password: $password) {
            _id
            token
            fullName
        }
    }
`;
