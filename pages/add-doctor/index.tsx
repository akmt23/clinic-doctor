import React, { useEffect } from "react";
import TabsHeadForStaff from "src/plugins/tab-routing/components/TabsHeadForStaff";
import { useTabRouting } from "src/plugins/tab-routing/hooks/useTabRouting";
import Link from "next/link";
import Router from "next/router";

const AddDoctorPage = () => {
    useEffect(() => {
        Router.push("add-doctor?route=personal-data");
    }, []);
    const routes: TabRoute[] = [
        {
            slug: "personal-data",
            label: "1. Личные данные",
            component: <PersonalInfo />,
        },
        {
            slug: "contact-info",
            label: "2. Контактная информация",
            component: <ContactInfo />,
        },
        {
            slug: "professional-info",
            label: "3. Профессиональная информация",
            component: <ProfessionalInfo />,
        },
        {
            slug: "education",
            label: "4. Образование",
            component: <EducationInfo />,
        },
        {
            slug: "qualification",
            label: "5. Курсы повышения квалификации",
            component: <Qualification />,
        },
        {
            slug: "previous-jobs",
            label: "6. Предыдущие места работы",
            component: <PrevJobInfo />,
        },
        {
            slug: "languages",
            label: "7. Языки",
            component: <LanguagesInfo />,
        },
        {
            slug: "hobby",
            label: "8. Хобби",
            component: <HobbyInfo />,
        },
    ];
    const TabBody = useTabRouting({ routes });
    return (
        <div>
            <div className="text-sm breadcrumbs text-base-300">
                <ul>
                    <li>
                        <a>Все сотрудники</a>
                    </li>
                    <li>
                        <a>Добавление врача</a>
                    </li>
                </ul>
            </div>
            <div>
                <p className="text-4xl font-bold">Добавление врача</p>
            </div>
            <div className="flex mt-5 gap-4">
                <div className="flex-1">
                    <div className="rounded-2xl bg-white py-5 px-7">
                        {TabBody}
                    </div>
                </div>
                <div className="flex-1">
                    <TabsHeadForStaff routes={routes}></TabsHeadForStaff>
                </div>
            </div>
        </div>
    );
};

const PersonalInfo = () => {
    return (
        <div className="space-y-3">
            <p>1. Личные данные</p>
            <div className="space-y-2">
                <div>
                    <p>Имя</p>
                    <input
                        type="text"
                        placeholder="Иван"
                        className="bg-gray-100 focus:bg-white w-full input input-ghost border focus:border-pink-purple"
                    />
                </div>
                <div>
                    <p>Фамилия</p>
                    <input
                        type="text"
                        placeholder="Иванов"
                        className="bg-gray-100 focus:bg-white w-full input input-ghost border focus:border-pink-purple"
                    />
                </div>
                <div>
                    <p>Отчество</p>
                    <input
                        type="text"
                        placeholder="Иванович"
                        className="bg-gray-100 focus:bg-white w-full input input-ghost border focus:border-pink-purple"
                    />
                </div>
                <div>
                    <p>Дата рождения</p>
                    <input
                        type="text"
                        placeholder="Иван"
                        className="bg-base-200 w-full input input-ghost"
                    />
                </div>
                <div className="space-y-2">
                    <p>Пол</p>
                    <label className="cursor-pointer flex items-center space-x-2">
                        <input
                            type="radio"
                            name="opt"
                            className="radio"
                            value=""
                        />
                        <span className="label-text">Мужской</span>
                    </label>
                    <label className="cursor-pointer flex items-center space-x-2">
                        <input
                            type="radio"
                            name="opt"
                            className="radio"
                            value=""
                        />
                        <span className="label-text">Женский</span>
                    </label>
                </div>
            </div>
            <div className="flex justify-end">
                <Link href="/add-doctor?route=contact-info">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Продолжить
                    </button>
                </Link>
            </div>
        </div>
    );
};

const ContactInfo = () => {
    return (
        <div className="space-y-3">
            <p>2. Контактная информация</p>
            <div className="space-y-2">
                <div>
                    <p>Рабочий номер телефона</p>
                    <input
                        type="tel"
                        placeholder="Иван"
                        className="bg-gray-100 focus:bg-white w-full input input-ghost border focus:border-pink-purple"
                    />
                </div>
                <div>
                    <p>Электронная почта</p>
                    <input
                        type="email"
                        placeholder="Иванов"
                        className="bg-gray-100 focus:bg-white w-full input input-ghost border focus:border-pink-purple"
                    />
                </div>
            </div>
            <div className="flex justify-between">
                <Link href="/add-doctor?route=personal-data">
                    <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                        Назад
                    </button>
                </Link>
                <Link href="/add-doctor?route=professional-info">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Продолжить
                    </button>
                </Link>
            </div>
        </div>
    );
};

const ProfessionalInfo = () => {
    return (
        <div className="space-y-3">
            <p>3. Профессиональная информация</p>
            <div className="space-y-2">
                <div>
                    <p>Специализация 1</p>
                    <select className="select w-full py-3 bg-gray-100">
                        <option>Название специализации</option>
                        <option>Терапевт</option>
                        <option>Андролог</option>
                        <option>Уролог</option>
                    </select>
                </div>
                <div className="flex items-center">
                    <div className="h-px w-full bg-gray-100"></div>
                    <div>
                        <button className="text-xs text-pink-purple">
                            Добавить специализацию
                        </button>
                    </div>
                    <div className="h-px w-full bg-gray-100"></div>
                </div>
                <div className="space-y-2">
                    <p>Принимает</p>
                    <label className="cursor-pointer flex items-center space-x-2">
                        <input
                            type="radio"
                            name="opt"
                            className="radio"
                            value=""
                        />
                        <span className="label-text">Только взрослых</span>
                    </label>
                    <label className="cursor-pointer flex items-center space-x-2">
                        <input
                            type="radio"
                            name="opt"
                            className="radio"
                            value=""
                        />
                        <span className="label-text">Только детей</span>
                    </label>
                    <label className="cursor-pointer flex items-center space-x-2">
                        <input
                            type="radio"
                            name="opt"
                            className="radio"
                            value=""
                        />
                        <span className="label-text">Взрослых и детей</span>
                    </label>
                </div>
                <div className="">
                    <p>Стаж работы</p>
                    <div className="flex gap-8">
                        <div className="flex-1 bg-gray-100 p-3 flex rounded">
                            <input
                                type="text"
                                placeholder="___"
                                className="bg-gray-100 focus:outline-none w-2/12"
                            />
                            <p>лет</p>
                        </div>
                        <div className="flex-1"></div>
                    </div>
                </div>
                <div className="">
                    <p>Процент от заработка</p>
                    <div className="flex gap-2">
                        <div className="flex-1 bg-gray-100 p-3 flex rounded space-x-2">
                            <p className="text-gray-400">Врачу</p>
                            <p className="text-gray-400">-</p>
                            <div className="flex">
                                <input
                                    type="text"
                                    placeholder="_____"
                                    className="bg-gray-100 focus:outline-none w-2/12"
                                />
                                <p>%</p>
                            </div>
                        </div>
                        <div className="flex-1 bg-gray-100 p-3 flex rounded space-x-2">
                            <p className="text-gray-400">Клинике</p>
                            <p className="text-gray-400">-</p>
                            <div className="flex">
                                <input
                                    type="text"
                                    placeholder="_____"
                                    className="bg-gray-100 focus:outline-none w-2/12"
                                />
                                <p>%</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="flex justify-between">
                    <Link href="/add-doctor?route=contact-info">
                        <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                            Назад
                        </button>
                    </Link>
                    <Link href="/add-doctor?route=education">
                        <button className="py-3 px-5 bg-pink-purple text-white rounded">
                            Продолжить
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    );
};

const EducationInfo = () => {
    return (
        <div className="space-y-3">
            <p>4. Образование</p>
            <p className="text-gray-400">
                Заполнить информацию об образовании врач может сам в своем
                личном кабинете.
            </p>
            <div className="space-y-2">
                <p className="text-xl">Учебное заведение</p>
                <div>
                    <p>Название</p>
                    <input
                        type="text"
                        placeholder="Название"
                        className="input bg-gray-100 w-full"
                    />
                </div>
                <div>
                    <p>Специальность</p>
                    <input
                        type="text"
                        placeholder="Специальность"
                        className="input bg-gray-100 w-full"
                    />
                </div>
                <div>
                    <p>Года обучения</p>
                    <div className="flex items-center space-x-2">
                        <input
                            type="text"
                            placeholder="C"
                            className="input bg-gray-100"
                        />
                        <p>-</p>
                        <input
                            type="text"
                            placeholder="По"
                            className="input bg-gray-100"
                        />
                    </div>
                </div>
            </div>
            <div className="flex items-center">
                <div className="h-px w-full bg-gray-100"></div>
                <div>
                    <button className="text-xs text-pink-purple">
                        Добавить учебное заведение
                    </button>
                </div>
                <div className="h-px w-full bg-gray-100"></div>
            </div>
            <div className="flex justify-between">
                <Link href="/add-doctor?route=professional-info">
                    <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                        Назад
                    </button>
                </Link>
                <Link href="/add-doctor?route=qualification">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Продолжить
                    </button>
                </Link>
            </div>
        </div>
    );
};

const Qualification = () => {
    return (
        <div className="space-y-3">
            <p>5. Курсы повышения квалификации</p>
            <p className="text-gray-400">
                Заполнить информацию о курсах врач может сам в своем личном
                кабинете.
            </p>
            <div className="space-y-2">
                <p className="text-xl">Курс повышения квалификации</p>
                <div>
                    <p>Название</p>
                    <input
                        type="text"
                        placeholder="Название"
                        className="input bg-gray-100 w-full"
                    />
                </div>
                <div>
                    <p>Специальность</p>
                    <input
                        type="text"
                        placeholder="Специальность"
                        className="input bg-gray-100 w-full"
                    />
                </div>
                <div>
                    <p>Года обучения</p>
                    <div className="flex items-center space-x-2">
                        <input
                            type="text"
                            placeholder="C"
                            className="input bg-gray-100"
                        />
                        <p>-</p>
                        <input
                            type="text"
                            placeholder="По"
                            className="input bg-gray-100"
                        />
                    </div>
                </div>
            </div>
            <div className="flex items-center">
                <div className="h-px w-full bg-gray-100"></div>
                <div>
                    <button className="text-xs text-pink-purple">
                        Добавить курс
                    </button>
                </div>
                <div className="h-px w-full bg-gray-100"></div>
            </div>
            <div className="flex justify-between">
                <Link href="/add-doctor?route=education">
                    <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                        Назад
                    </button>
                </Link>
                <Link href="/add-doctor?route=previous-jobs">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Продолжить
                    </button>
                </Link>
            </div>
        </div>
    );
};

const PrevJobInfo = () => {
    return (
        <div className="space-y-3">
            <p>6. Предыдущие места работы</p>
            <p className="text-gray-400">
                Заполнить информацию о предыдущих местах работы врач может сам в
                своем личном кабинете.
            </p>
            <div className="space-y-2">
                <p className="text-xl">Место работы</p>
                <div>
                    <p>Название</p>
                    <input
                        type="text"
                        placeholder="Название"
                        className="input bg-gray-100 w-full"
                    />
                </div>
                <div>
                    <p>Специальность</p>
                    <input
                        type="text"
                        placeholder="Специальность"
                        className="input bg-gray-100 w-full"
                    />
                </div>
                <div>
                    <p>Года обучения</p>
                    <div className="flex items-center space-x-2">
                        <input
                            type="text"
                            placeholder="C"
                            className="input bg-gray-100"
                        />
                        <p>-</p>
                        <input
                            type="text"
                            placeholder="По"
                            className="input bg-gray-100"
                        />
                    </div>
                </div>
            </div>
            <div className="flex items-center">
                <div className="h-px w-full bg-gray-100"></div>
                <div>
                    <button className="text-xs text-pink-purple">
                        Добавить места работы
                    </button>
                </div>
                <div className="h-px w-full bg-gray-100"></div>
            </div>
            <div className="flex justify-between">
                <Link href="/add-doctor?route=qualification">
                    <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                        Назад
                    </button>
                </Link>
                <Link href="/add-doctor?route=languages">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Продолжить
                    </button>
                </Link>
            </div>
        </div>
    );
};

const LanguagesInfo = () => {
    return (
        <div className="space-y-3">
            <p>7. Языки</p>
            <p className="text-gray-400">
                Заполнить информацию о языках врач может сам в своем личном
                кабинете.
            </p>
            <div className="space-y-2">
                <div>
                    <p className="text-xl">Первый</p>
                    <div className="flex items-center space-x-2">
                        <input
                            type="text"
                            placeholder="Язык"
                            className="input bg-gray-100 w-full"
                        />
                    </div>
                </div>
            </div>
            <div className="flex justify-between">
                <Link href="/add-doctor?route=previous-jobs">
                    <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                        Назад
                    </button>
                </Link>
                <Link href="/add-doctor?route=hobby">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Продолжить
                    </button>
                </Link>
            </div>
        </div>
    );
};

const HobbyInfo = () => {
    return (
        <div className="space-y-3">
            <p>8. Хобби</p>
            <p className="text-gray-400">
                Заполнить информацию о хобби врач может сам в своем личном
                кабинете.
            </p>
            <div className="space-y-2">
                <div>
                    <p className="text-xl">Первый</p>
                    <div className="flex items-center space-x-2">
                        <input
                            type="text"
                            placeholder="Язык"
                            className="input bg-gray-100 w-full"
                        />
                    </div>
                </div>
            </div>
            <div className="flex justify-between">
                <Link href="/add-doctor?route=previous-jobs">
                    <button className="py-3 px-5 border border-pink-purple text-pink-purple rounded">
                        Назад
                    </button>
                </Link>
                <Link href="/add-doctor?route=hobby">
                    <button className="py-3 px-5 bg-pink-purple text-white rounded">
                        Завершить регистрацию
                    </button>
                </Link>
            </div>
        </div>
    );
};

export default AddDoctorPage;
