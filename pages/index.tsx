import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import getAdminLayout from "components/layouts/adminLayout";
import { useQuery } from "@apollo/client";

import { GET_UPCOMING_BOOKINGS, GET_DOCTOR_BY_ID } from "graphql/query";
import { useDoctor } from "@recoil/hooks";

import { getTime, getDayName, getMonthAndDay } from "src/helper";

const MainPage = () => {
    const [doctor, { setCurrentDoctor }] = useDoctor();
    const router = useRouter();
    const [token, setToken] = useState("");
    const [doctorId, setDoctorId] = useState("");

    const {
        loading: loadingBooking,
        error: errorBooking,
        data: dataBooking,
    } = useQuery(GET_UPCOMING_BOOKINGS, {
        variables: {
            page: 1,
        },
        context: {
            headers: {
                Authorization: token,
            },
        },
    });
    const { loading: loadingDoctor, error: errorDoctor } = useQuery(
        GET_DOCTOR_BY_ID,
        {
            variables: {
                doctorId: doctorId,
            },
            context: {
                headers: {
                    Authorization: token,
                },
            },
            onCompleted: (data) => {
                setCurrentDoctor(data.getDoctorByID);
            },
        },
    );
    useEffect(() => {
        const localDoctorId = localStorage.getItem("DOCTOR_ID");
        const localToken = localStorage.getItem("JWT");
        if (!localToken) router.push("/login");
        if (localToken) setToken(localToken);
        if (localDoctorId) setDoctorId(localDoctorId);
    }, []);
    const date = {
        day: getMonthAndDay(new Date().toISOString()),
        time: getTime(new Date().toISOString()),
    };
    return (
        <div>
            <div className="flex justify-between">
                <p className="text-3xl font-bold">
                    Здравствуйте, {doctor.fullName}
                </p>
                <p className=" text-gray-400 self-center">
                    {date.day}, {date.time}
                </p>
            </div>
            <hr className="my-3" />
            <div className="space-y-4">
                <div className="grid grid-cols-12 gap-4">
                    <RegistrationCard className="col-start-1 col-end-7 p-5 space-y-2 rounded">
                        <div className="flex justify-between">
                            <p>Ближайшая запись</p>
                            <p>Сегодня, 11:30</p>
                        </div>
                        <div className="flex rounded p-3 bg-white justify-between shadow-md">
                            <div className="flex space-x-2">
                                <img
                                    src="http://www.caribbeangamezone.com/wp-content/uploads/2018/03/avatar-placeholder.png"
                                    alt=""
                                    className="h-12 w-12"
                                />
                                <div>
                                    <p className="text-xl ">Иванов Иван</p>
                                    <p>+7 708 382 24 61</p>
                                </div>
                            </div>
                            <div className="text-gray-400">
                                <p>Первичный прием</p>
                                <p>Мужчина, 22 года</p>
                            </div>
                        </div>
                        <div className="flex space-x-2">
                            <button className="flex-1 bg-pink-purple text-white py-1 rounded">
                                Начать запись
                            </button>
                            <button className="flex-1 border border-pink-purple text-pink-purple py-1 rounded">
                                Отменить
                            </button>
                        </div>
                    </RegistrationCard>
                    <FinanceCard className="p-5 space-y-14 rounded col-start-7 col-end-10">
                        <div className="flex justify-between">
                            <p>Финансы</p>
                            <button>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
                                    />
                                </svg>
                            </button>
                        </div>
                        <div className="">
                            <div className="flex">
                                <p className="text-main-green text-4xl font-bold">
                                    2500<span>₸</span>
                                </p>
                            </div>
                            <div>
                                <p>за сегодня</p>
                            </div>
                        </div>
                    </FinanceCard>
                    <PatientStatCard className="p-5 space-y-14 rounded col-start-10 col-end-13">
                        <div className="flex justify-between">
                            <p>Принято пациентов в этом месяце</p>
                        </div>
                        <div className="">
                            <div className="flex">
                                <p className="text-4xl font-bold">70</p>
                            </div>
                            <div>
                                <p>человек</p>
                            </div>
                        </div>
                    </PatientStatCard>
                </div>
                <div className="grid grid-cols-12 gap-4">
                    <ScheduleCard className="p-5 rounded col-start-1 col-end-7">
                        <div className="flex justify-between">
                            <p>
                                Расписание на сегодня{" "}
                                <span className="text-gray-400">
                                    {" "}
                                    - 10 пациентов{" "}
                                </span>
                            </p>
                            <div className="space-x-2">
                                <button>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-6 w-6"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                        />
                                    </svg>
                                </button>
                                <button>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-6 w-6"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                                        />
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                        />
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div className="h-96 overflow-auto space-y-2">
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                            <PatientCard />
                        </div>
                    </ScheduleCard>
                    <ScheduleCard className="p-5 rounded col-start-7 col-end-13 space-y-2">
                        <p>Прошедшие записи</p>
                        <div className="h-96 overflow-auto space-y-2">
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                            <PostRecordCard />
                        </div>
                    </ScheduleCard>
                </div>
            </div>
        </div>
    );
};

const PatientCard = () => {
    return (
        <div className="rounded shadow-md grid grid-cols-12 bg-white p-2">
            <div className="flex justify-center col-start-1 col-end-3">
                <p className="">11:30</p>
            </div>
            <div className="col-start-3 col-end-8">
                <p>Иванов Иван</p>
                <p className="text-gray-400">Первичный прием</p>
                <p className="text-gray-400">Мужчина, 22 года</p>
            </div>
            <div className="flex items-center col-start-8 col-end-12">
                <button className="border border-pink-purple text-pink-purple py-2 px-12 rounded hover:bg-pink-purple hover:text-white">
                    Начать прием
                </button>
            </div>
        </div>
    );
};

const PostRecordCard = () => {
    return (
        <div className="rounded shadow-md grid grid-cols-12 bg-white p-2">
            <div className="flex justify-center col-start-1 col-end-3">
                <p className="">11:30</p>
            </div>
            <div className="col-start-3 col-end-8">
                <p>Иванов Иван</p>
                <p className="text-gray-400">Первичный прием</p>
                <p className="text-gray-400">Мужчина, 22 года</p>
            </div>
            <div className="flex items-center justify-end col-start-8 col-end-12">
                <p className="text-main-green">Документы загружены</p>
            </div>
        </div>
    );
};

const RegistrationCard = styled.div`
    background-color: #faf5ff;
`;

const ScheduleCard = styled.div`
    background-color: #f7f7f7;
`;

const FinanceCard = styled.div`
    background-color: #f0fff2;
`;

const PatientStatCard = styled.div`
    background-color: #f0f3ff;
`;

export default MainPage;
