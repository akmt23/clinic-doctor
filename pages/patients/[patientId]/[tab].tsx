import getAdminLayout from "components/layouts/adminLayout";
import { AnimatePresence, motion } from "framer-motion";
import React, { useMemo, useState } from "react";
import TabsHead from "src/plugins/tab-routing/components/TabsHead";
import { useTabRouting } from "src/plugins/tab-routing/hooks/useTabRouting";

const PatientPage = () => {
    const routes: TabRoute[] = [
        {
            slug: "adult",
            label: "Консультационные листы",
            component: <ConsultationalLists />,
        },
        {
            slug: "children",
            label: "Анализы",
            component: <div>Услуги для детей</div>,
        },
        {
            slug: "asdf",
            label: "Результаты исследований",
            component: <div>Услуги для детей</div>,
        },
        {
            slug: "fdsa",
            label: "История",
            component: <div>Услуги для детей</div>,
        },
    ];
    const TabBody = useTabRouting({ routes });
    const [showSessionModal, setShowSessionModal] = useState(false);
    return (
        <div className="p-5 bg-white rounded-2xl">
            <div className="text-sm breadcrumbs text-base-300">
                <ul>
                    <li>
                        <a>Все пациенты</a>
                    </li>
                    <li>
                        <a>Иван Иванов Иванович</a>
                    </li>
                </ul>
            </div>

            <div className="flex items-center justify-between">
                <h1 className=" text-4xl font-bold mb-2">
                    Иван Иванов Иванович
                </h1>
                <div className="flex items-center">
                    <button className="btn btn-ghost btn-sm capitalize font-normal text-error mr-2">
                        Отменить прием
                    </button>
                    <button
                        onClick={() => setShowSessionModal(true)}
                        className="btn btn-primary btn-sm px-6 font-normal capitalize"
                    >
                        Начать прием
                    </button>
                </div>
            </div>
            <div className="w-full border-b-2 border-base-200 mb-4"></div>
            <div className="mb-4">
                <div className="grid grid-cols-8 w-full gap-4 items-center">
                    <div className="avatar col-span-1">
                        <div className="rounded-full w-full">
                            <img src="http://daisyui.com/tailwind-css-component-profile-1@94w.png" />
                        </div>
                    </div>
                    <div className="h-full col-span-3">
                        <div className="h-full w-full bg-base-200 rounded-lg p-4">
                            <div className="flex justify-between items-center mb-2">
                                <span className=" text-xl font-medium">
                                    Основное
                                </span>
                                <div>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-6 w-6 text-primary"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                        />
                                    </svg>
                                </div>
                            </div>
                            <div className="flex flex-col space-y-1">
                                <p>
                                    <span className="text-base-300 mr-1">
                                        Пол:
                                    </span>
                                    <span>Мужской</span>
                                </p>
                                <p>
                                    <span className="text-base-300 mr-1">
                                        Дата рождение:
                                    </span>
                                    <span>23.11.2000</span>
                                </p>
                                <p>
                                    <span className="text-base-300 mr-1">
                                        Возраст:
                                    </span>
                                    <span>22 года</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="h-full bg-base-200 rounded-lg col-span-2 p-4">
                        <div className="flex justify-between items-center mb-2">
                            <span className=" text-xl font-medium">
                                Контакты
                            </span>
                            <div>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6 text-primary"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                    />
                                </svg>
                            </div>
                        </div>
                        <div className="flex flex-col items-start space-y-1">
                            <div className="flex items-center justify-start">
                                <p className="text-base-300 mr-1">Тел:</p>
                                <p>+7 708 382 24 61</p>
                            </div>
                            <div className="flex items-center justify-start">
                                <p className="text-base-300 mr-1">Почта:</p>
                                <p className="truncate w-1/2">
                                    jean2wwwфывафываыаф@gmail.com
                                </p>
                            </div>
                            <div className="flex items-center justify-start">
                                <p className="text-base-300 mr-1">Возраст:</p>
                                <p>22 года</p>
                            </div>
                        </div>
                    </div>
                    <div className="h-full bg-base-200 rounded-lg col-span-2 p-4">
                        <div className="flex justify-between items-center mb-2">
                            <span className=" text-xl font-medium">
                                Особенности
                            </span>
                            <div>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6 text-primary"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                    />
                                </svg>
                            </div>
                        </div>
                        <div className="flex flex-col items-start space-y-1">
                            <div className="flex items-center justify-start">
                                <p className="text-base-300 mr-1">Аллергии:</p>
                                <p>Нет</p>
                            </div>
                        </div>
                    </div>
                    <div className="h-full bg-base-200 rounded-lg col-span-2"></div>
                </div>
            </div>
            <TabsHead routes={routes}></TabsHead>
            <div className="mt-4">{TabBody}</div>
            <AppointmentSessionModal
                active={showSessionModal}
                onClose={() => setShowSessionModal(false)}
            ></AppointmentSessionModal>
        </div>
    );
};

const AppointmentSessionModal: React.FC<{
    active: boolean;
    onClose: () => void;
}> = ({ active, onClose }) => {
    return (
        <div id="my-modal" className={`modal ${active && "modal-open"}`}>
            <div className="modal-box max-w-5xl overflow-hidden">
                <div className="flex justify-between items-start">
                    <span className="font-bold text-3xl w-1/2">
                        Иван И.Е. — первичный прием Кардиолога 10.09.2021
                    </span>
                    <div className="flex flex-col w-52 space-y-1">
                        <span className="font-bold text-sm">
                            Преием идет (00:23)
                        </span>
                        <button className="btn btn-secondary btn-sm">
                            Скрыть окно на время
                        </button>
                        <span className="text-base-300 text-xs">
                            Вы сможете вернуться обратно к заполнению
                        </span>
                    </div>
                </div>
                {/* <div className="p-4 bg-base-200 rounded-lg">
                    <div className="flex flex-col">
                        <span className="font-medium text-lg mb-2">
                            Выбор шаблона консультационного листа
                        </span>
                        <select className="select select-bordered w-full font-light">
                            <option disabled="disabled" selected="selected">
                                Гипертрофическая кардиомиопатия
                            </option>
                            <option>telekinesis</option>
                            <option>time travel</option>
                            <option>invisibility</option>
                        </select>
                    </div>
                </div> */}
                <SessionContent></SessionContent>
                <div className="modal-action flex flex-col m-0">
                    <div>
                        <div className=" border-b-2 border-base-200 w-full mb-2"></div>
                        <div className="flex justify-between items-center w-full mr-1">
                            <button
                                className="btn btn-error "
                                onClick={onClose}
                            >
                                Отменить прием
                            </button>
                            <button className="btn btn-primary px-6">
                                Далее
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

const SessionContent = () => {
    const sessionSteps = [
        {
            label: "Жалобы и анамнез заболевания",
            formComponent: <IllnessReason></IllnessReason>,
        },
        {
            label: "Осмотр",
            formComponent: <GeneralReviewForm></GeneralReviewForm>,
        },
        {
            label: "Результаты исследований",
            formComponent: <ResearchResultForm></ResearchResultForm>,
        },
        {
            label: "Диагноз",
            formComponent: <DiagnosesForm></DiagnosesForm>,
        },
        {
            label: "Назначения",
            formComponent: <CuringPlanForm></CuringPlanForm>,
        },
        {
            label: "Справки",
            formComponent: <ReferenceForm></ReferenceForm>,
        },
        {
            label: "Завершение приема",
            formComponent: <SessionCompletion></SessionCompletion>,
        },
    ];

    const [sessionStepIndex, setSessionStepIndex] = useState(4);
    return (
        <div className="">
            <div className="flex items-start">
                <aside>
                    <ul className="menu w-40">
                        {sessionSteps.map((step, i) => (
                            <li>
                                <a
                                    onClick={() => setSessionStepIndex(i)}
                                    style={{ alignItems: "flex-start" }}
                                    className={`flex items-start justify-start rounded-lg ${
                                        i === sessionStepIndex && "active"
                                    }`}
                                >
                                    <span className="mr-2">{i + 1}.</span>
                                    <span>{step.label}</span>
                                </a>
                            </li>
                        ))}
                    </ul>
                </aside>
                <section className="flex-grow p-4 flex flex-col h-xxl max-h-xxl overflow-y-scroll">
                    <AnimatePresence exitBeforeEnter>
                        {sessionSteps[sessionStepIndex].formComponent}
                    </AnimatePresence>
                </section>
            </div>
        </div>
    );
};

const FormAnimatedContainer: React.FC = (props) => {
    return (
        <motion.div
            initial={{ opacity: 0, scale: 0.75 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0, scale: 0 }}
        >
            {props.children}
        </motion.div>
    );
};

const CuringPlanForm = () => {
    return (
        <FormAnimatedContainer>
            <div className="flex items-center mb-2">
                <h3 className="font-medium text-xl whitespace-nowrap mr-2">
                    Заверешение приема
                </h3>
                <hr className="w-full border-base-300 flex-grow"></hr>
            </div>
            <div className="w-full bg-base-200 rounded-lg p-4 mb-2">
                <div className="flex justify-between items-center">
                    <div className="flex items-center">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5 mr-2 mb-1"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
                        </svg>
                        <span className="font-bold h-auto">
                            Консультации специалистов
                        </span>
                    </div>
                    <div className="flex items-center">
                        <button className="btn btn-primary btn-outline border-none">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            Записать к себе
                        </button>
                        <button className="btn btn-primary btn-outline border-none">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            Добавить
                        </button>
                    </div>
                </div>
            </div>
            <div className="w-full bg-base-200 rounded-lg p-4 ">
                <div className="flex justify-between items-center mb-2">
                    <div className="flex items-center">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5 mr-2 "
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z" />
                        </svg>
                        <span className="font-bold h-auto">
                            Инструментальная диагностика
                        </span>
                    </div>
                    <div className="flex items-center">
                        <button className="btn btn-primary btn-outline border-none">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            Добавить
                        </button>
                    </div>
                </div>
                <DiagnosticsItem></DiagnosticsItem>
            </div>
            <div className="mt-8 mb-4">
                <div className="flex items-center justify-between">
                    <div className="flex items-center">
                        <p className="text-lg font-medium mr-2">План лечения</p>
                        <div className="btn-group">
                            <button className="btn btn-active">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                    />
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                    />
                                </svg>
                            </button>
                            <button className="btn  btn-outline btn-primary">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div>
                        <select className="select flex-grow bg-base-200 w-64 font-light mr-2">
                            <option disabled="disabled" selected="selected">
                                Новый шаблон
                            </option>
                        </select>
                        <button className="btn btn-primary btn-outline">
                            Сохранить
                        </button>
                    </div>
                </div>
            </div>
            <div className="w-full bg-base-200 rounded-lg p-4 mb-2">
                <div className="flex justify-between items-center">
                    <div className="flex items-center">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M7 2a1 1 0 00-.707 1.707L7 4.414v3.758a1 1 0 01-.293.707l-4 4C.817 14.769 2.156 18 4.828 18h10.343c2.673 0 4.012-3.231 2.122-5.121l-4-4A1 1 0 0113 8.172V4.414l.707-.707A1 1 0 0013 2H7zm2 6.172V4h2v4.172a3 3 0 00.879 2.12l1.027 1.028a4 4 0 00-2.171.102l-.47.156a4 4 0 01-2.53 0l-.563-.187a1.993 1.993 0 00-.114-.035l1.063-1.063A3 3 0 009 8.172z"
                                clipRule="evenodd"
                            />
                        </svg>
                        <span className="font-bold h-auto">
                            Медикаментозные назначения
                        </span>
                    </div>
                    <div className="flex items-center">
                        <button className="btn btn-primary btn-outline border-none">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            Добавить
                        </button>
                    </div>
                </div>
            </div>
        </FormAnimatedContainer>
    );
};

const DiagnosticsItem = () => {
    return (
        <div className="w-full bg-base-100 rounded-lg p-4 shadow-md">
            <div className="flex items-center justify-between">
                <div className="flex flex-col">
                    <span>Электрокардиография (ЭКГ)</span>
                    <span className="text-sm text-base-300">
                        15.09.2021 15:00
                    </span>
                </div>
                <div>
                    <button className="btn btn-primary py-2 btn-outline mr-2">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                            />
                        </svg>
                    </button>
                    <button className="btn btn-error py-2 bg-transparent text-warning hover:text-white">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                            />
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    );
};

const SessionCompletion = () => {
    return (
        <FormAnimatedContainer>
            <div className="w-full bg-base-200 rounded-lg p-4">
                <div className="flex justify-between items-center">
                    <h3 className="font-medium text-xl mb-4">
                        Заверешение приема
                    </h3>
                    <button className="btn btn-primary btn-outline">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5 mr-1"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M17 17h2a2 2 0 002-2v-4a2 2 0 00-2-2H5a2 2 0 00-2 2v4a2 2 0 002 2h2m2 4h6a2 2 0 002-2v-4a2 2 0 00-2-2H9a2 2 0 00-2 2v4a2 2 0 002 2zm8-12V5a2 2 0 00-2-2H9a2 2 0 00-2 2v4h10z"
                            />
                        </svg>
                        Печать
                    </button>
                </div>
            </div>
        </FormAnimatedContainer>
    );
};

const ReferenceForm = () => {
    return (
        <FormAnimatedContainer>
            <div className="w-full bg-base-200 rounded-lg p-4">
                <h3 className="font-medium text-xl mb-4">Справки</h3>
                <div className="form-control mb-2">
                    <label className="label">
                        <span className="label-text">Тип справки</span>
                    </label>
                    <select className="select select-bordered flex-grow border-transparent font-light mr-2">
                        <option disabled="disabled" selected="selected">
                            Не выбрано
                        </option>
                    </select>
                </div>
            </div>
        </FormAnimatedContainer>
    );
};

const DiagnosesForm = () => {
    return (
        <FormAnimatedContainer>
            <div className="w-full bg-base-200 rounded-lg p-4">
                <h3 className="font-medium text-xl mb-4">Диагноз</h3>
                <div className="btn-group mb-2">
                    <button className="btn btn-lg btn-active">
                        Окончательный
                    </button>
                    <button className="btn btn-lg btn-outline btn-primary">
                        Предварительный
                    </button>
                </div>
                <div className="form-control mb-2">
                    <label className="label">
                        <span className="label-text">Код по МКБ</span>
                    </label>
                    <select className="select select-bordered flex-grow border-transparent font-light mr-2">
                        <option disabled="disabled" selected="selected">
                            I10 — Гипертоническая болезнь первой степени, первой
                            стадии, риск средней
                        </option>
                    </select>
                </div>
                <div className="form-control mb-2">
                    <label className="label">
                        <span className="label-text">
                            Развернутый клинический диагноз
                        </span>
                    </label>
                    <textarea className="textarea h-24"></textarea>
                </div>
                <div className="form-control mb-2">
                    <label className="label">
                        <span className="label-text">Характер болезни</span>
                    </label>
                    <textarea className="textarea h-24"></textarea>
                </div>
            </div>
        </FormAnimatedContainer>
    );
};

const ResearchResultForm = () => {
    return (
        <FormAnimatedContainer>
            <h3 className="font-medium text-xl mb-4">Осмотр</h3>
            <div className="w-full bg-base-200 rounded-lg p-4">
                <div className="form-control mb-4">
                    <label className="label">
                        <span className="label-text">
                            Результаты исследования
                        </span>
                    </label>
                    <div className="flex items-center">
                        <select className="select select-bordered flex-grow border-transparent font-light mr-2">
                            <option disabled="disabled" selected="selected">
                                Новый шаблон
                            </option>
                        </select>
                        <button className="btn btn-primary btn-outline px-6 mr-2 flex items-center">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            Новый
                        </button>
                        <button className="btn btn-primary px-6 flex items-center">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M4 5a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V7a2 2 0 00-2-2h-1.586a1 1 0 01-.707-.293l-1.121-1.121A2 2 0 0011.172 3H8.828a2 2 0 00-1.414.586L6.293 4.707A1 1 0 015.586 5H4zm6 9a3 3 0 100-6 3 3 0 000 6z"
                                    clipRule="evenodd"
                                />
                            </svg>{" "}
                            Новый
                        </button>
                    </div>
                </div>
                <div className=" border-b-2 border-base-300 w-full mb-4"></div>
                <div className="form-control">
                    <textarea
                        className="textarea h-24"
                        placeholder="Описание"
                    ></textarea>
                </div>
            </div>
            <button className="btn btn-primary btn-outline w-full py-2 mt-4">
                + Добавить пункт осмотра
            </button>
        </FormAnimatedContainer>
    );
};

const GeneralReviewForm = () => {
    return (
        <FormAnimatedContainer>
            <h3 className="font-medium text-xl mb-4">Осмотр</h3>
            <div className="w-full bg-base-200 rounded-lg p-4">
                <div className="form-control mb-4">
                    <label className="label">
                        <span className="label-text">
                            Шаблон пункта осмотра
                        </span>
                    </label>
                    <div className="flex items-center">
                        <select className="select select-bordered flex-grow border-transparent font-light mr-2">
                            <option disabled="disabled" selected="selected">
                                Новый шаблон
                            </option>
                        </select>
                        <button className="btn btn-primary px-6 flex items-center">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5 mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            Новый
                        </button>
                    </div>
                </div>
                <div className=" border-b-2 border-base-300 w-full mb-4"></div>
                <div className="form-control">
                    <textarea
                        className="textarea h-24"
                        placeholder="Описание"
                    ></textarea>
                </div>
            </div>
            <button className="btn btn-primary btn-outline w-full mt-4">
                + Добавить пункт осмотра
            </button>
        </FormAnimatedContainer>
    );
};

const IllnessReason = () => {
    return (
        <FormAnimatedContainer>
            <h3 className="font-medium text-xl mb-4">
                Жалобы и анамнез заболевания
            </h3>
            <div className="w-full bg-base-200 rounded-lg p-4">
                <div className="form-control">
                    <label className="label">
                        <span className="label-text">Жалобы</span>
                    </label>
                    <textarea
                        className="textarea h-24"
                        placeholder="Жалобы заболевшего"
                    ></textarea>
                </div>
                <div className="form-control">
                    <label className="label">
                        <span className="label-text">Болеет в течение</span>
                    </label>
                    <div className="flex">
                        <input
                            type="number"
                            placeholder="Кол-во"
                            className="input mr-2"
                        />

                        <select className="select select-bordered w-full max-w-xs border-transparent font-light">
                            <option disabled="disabled" selected="selected">
                                Период
                            </option>
                            <option>Месяцев</option>
                            <option>Дней</option>
                            <option>Лет</option>
                        </select>
                    </div>
                </div>
                <div className="form-control">
                    <label className="label">
                        <span className="label-text">Анамнез заболевания</span>
                    </label>
                    <textarea
                        className="textarea h-24"
                        placeholder="Анамнез заболевания"
                    ></textarea>
                </div>
            </div>
        </FormAnimatedContainer>
    );
};

const ConsultationalLists = () => {
    return (
        <div className="grid grid-cols-2">
            <div className="bg-base-200 p-4 rounded-lg">
                <h3 className=" font-medium text-lg">Консультационные листы</h3>
                <div className="flex items-center justify-between mb-4">
                    <div className="dropdown dropdown-right dropdown-end">
                        <div
                            tabIndex={0}
                            className=" cursor-pointer flex items-center"
                        >
                            <span className="mr-1 text-sm"> По диагнозу </span>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clipRule="evenodd"
                                />
                            </svg>
                        </div>
                        <ul
                            tabIndex={0}
                            className="p-2 shadow menu dropdown-content bg-base-100 rounded-box w-52"
                        >
                            <li>
                                <a>Item 1</a>
                            </li>
                            <li>
                                <a>Item 2</a>
                            </li>
                            <li>
                                <a>Item 3</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <label className="cursor-pointer label">
                            <span className="text-sm mr-2">
                                Только мои осмотры
                            </span>
                            <input
                                type="checkbox"
                                checked={false}
                                className="checkbox"
                            />
                        </label>
                    </div>
                </div>
                <div className="text-base-300">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Inventore fugiat molestias esse delectus et. Recusandae
                    atque eum quos non dignissimos provident tenetur odit
                    expedita libero molestiae iusto, maiores autem rem?
                </div>
            </div>
        </div>
    );
};

export default PatientPage;
