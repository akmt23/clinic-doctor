import { patientArray } from "@core/mock/patiences";
import getAdminLayout from "components/layouts/adminLayout";
import React from "react";
import { ElevatedContainer } from "components/atoms/ElevatedContainer";
import { PatientEntity } from "@core/types/patient/IPatient";
import Link from "next/link";

const PatientsPage = () => {
    return (
        <div className="">
            <div className="flex justify-between">
                <h1 className="text-4xl font-bold mb-4">Пациенты</h1>
                <div className="form-control lg:w-1/2 mb-4">
                    <input
                        type="text"
                        placeholder="Поиск по имени"
                        className="input bg-white"
                    />
                </div>
            </div>
            <div className="w-full border-b-2 border-base-200 mb-4"></div>
            <div className="grid grid-cols-3 gap-4 bg-white p-5 rounded-xl">
                {patientArray.map((patient) => (
                    <PatientCard
                        key={patient?.id}
                        patient={patient}
                    ></PatientCard>
                ))}
            </div>
        </div>
    );
};

const PatientCard: React.FC<{ patient: PatientEntity }> = ({ patient }) => {
    return (
        <Link href={`/patients/${patient?.id}/home`}>
            <ElevatedContainer className="rounded-lg p-4 h-full cursor-pointer">
                <div className="flex items-center ">
                    <div className="avatar mr-4">
                        <div className="rounded-full w-20 h-20">
                            <img
                                width="80"
                                height="80"
                                src={patient?.avatarUrl}
                            />
                        </div>
                    </div>
                    <div className="flex flex-col justify-start">
                        <span className="text-2xl font-medium w-1/2">
                            {patient.fullName}
                        </span>
                        <span className="text-base-300">ИИН: 0123123212</span>
                    </div>
                </div>
            </ElevatedContainer>
        </Link>
    );
};

export default PatientsPage;
