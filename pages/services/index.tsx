import React, { useState } from "react";
import { ElevatedContainer } from "components/atoms/ElevatedContainer";

const ServicesPage = () => {
    const [showAddSpezModal, setShowAddSpezModal] = useState(false);
    const [showAddServiceModal, setShowAddServiceModal] = useState(false);
    return (
        <div>
            <div className="space-y-5">
                <div className="flex justify-between">
                    <p className="text-4xl font-bold">Услуги</p>
                    <div className="flex space-x-5">
                        <input
                            type="text"
                            placeholder="Поиск по названию..."
                            className="input bg-white"
                        />
                        <button
                            onClick={() => setShowAddSpezModal(true)}
                            className="flex bg-pink-purple p-2 text-white rounded items-center"
                        >
                            Добавить специализацию{" "}
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                        </button>
                    </div>
                </div>
                <div className="p-5 rounded-2xl bg-white space-y-3">
                    <div className="flex justify-between">
                        <p className="text-2xl font-medium">Гинекология</p>
                        <button
                            onClick={() => {
                                setShowAddServiceModal(true);
                            }}
                            className="text-pink-purple px-3 py-1 rounded hover:bg-pink-purple hover:text-white"
                        >
                            Добавить услугу
                        </button>
                    </div>
                    <div className="flex">
                        <div className="flex-1">
                            <p className="text-gray-400">Название услуги</p>
                        </div>
                        <div className="flex justify-between flex-1">
                            <p className="text-gray-400">Цена, тенге</p>
                            <p className="text-gray-400">Консультация</p>
                            <p className="text-gray-400">Длительность, мин</p>
                        </div>
                    </div>
                    <hr />
                    <div className="space-y-3">
                        <ServiceCard
                            name="Первичный осмотр гинеколога"
                            price="6000"
                            consult="Не нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Первичный осмотр гинеколога"
                            price="6000"
                            consult="Нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Первичный осмотр гинеколога"
                            price="6000"
                            consult="Не нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="осмотр гинеколога"
                            price="6000"
                            consult="Нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Первичный осмотр гинеколога"
                            price="6000"
                            consult="Не нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Первичный осм"
                            price="6000"
                            consult="Нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Пе гинеколога"
                            price="6000"
                            consult="Не нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Первичный осмотр гинеколога"
                            price="6000"
                            consult="Нужна"
                            duration="30"
                        />
                        <ServiceCard
                            name="Первичный осмотр гинеколога"
                            price="6000"
                            consult="Не нужна"
                            duration="30"
                        />
                    </div>
                </div>
            </div>
            <AddSpecializationModal
                active={showAddSpezModal}
                onClose={() => setShowAddSpezModal(false)}
            ></AddSpecializationModal>

            <AddServiceModal
                active={showAddServiceModal}
                onClose={() => setShowAddServiceModal(false)}
            ></AddServiceModal>
        </div>
    );
};

interface ServicesProps {
    name: String;
    price: String;
    consult: String;
    duration: String;
}

const ServiceCard: React.FC<ServicesProps> = ({
    name,
    price,
    consult,
    duration,
}) => {
    return (
        <ElevatedContainer className="rounded cursor-pointer">
            <div className="flex bg-white p-3 rounded">
                <div className="flex-1">
                    <p>{name}</p>
                </div>
                <div className="flex-1 grid grid-col-12">
                    <p className="col-start-1 col-end-5">{price}</p>
                    <p className="col-start-6 col-end-10">{consult}</p>
                    <p className="col-start-10 col-end-13">{duration}</p>
                </div>
            </div>
        </ElevatedContainer>
    );
};

const AddSpecializationModal: React.FC<{
    active: boolean;
    onClose: () => void;
}> = ({ active, onClose }) => {
    return (
        <div id="my-modal" className={`modal ${active && "modal-open"}`}>
            <div className="modal-box max-w-2xl overflow-hidden space-y-5">
                <div className="flex justify-between">
                    <p className="text-4xl font-bold">
                        Добавление специализации
                    </p>
                    <button onClick={onClose}>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </button>
                </div>
                <div className="space-y-3">
                    <p className="text-2xl font-semibold">
                        Название специализации
                    </p>
                    <input
                        type="text"
                        className="p-2 border border-gray-100 focus:outline-none focus:border-pink-purple rounded w-full"
                        placeholder="Название"
                    />
                </div>
                <div className="flex justify-end">
                    <button className="bg-pink-purple p-3 rounded text-white">
                        Добавить специализацию
                    </button>
                </div>
            </div>
        </div>
    );
};

const AddServiceModal: React.FC<{
    active: boolean;
    onClose: () => void;
}> = ({ active, onClose }) => {
    return (
        <div id="my-modal" className={`modal ${active && "modal-open"}`}>
            <div className="modal-box max-w-2xl overflow-hidden space-y-5">
                <div className="flex justify-between">
                    <p className="text-4xl font-bold">Добавление услуги</p>
                    <button onClick={onClose}>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </button>
                </div>
                <div className="space-y-3">
                    <div className="space-y-3">
                        <p className="text-xl">Название услуги</p>
                        <input
                            type="text"
                            className="p-2 border border-gray-100 focus:outline-none focus:border-pink-purple rounded w-full"
                            placeholder="Название"
                        />
                    </div>
                    <div className="space-y-3">
                        <p className="text-xl">Специализация</p>
                        <input
                            type="text"
                            className="p-2 border border-gray-100 focus:outline-none focus:border-pink-purple rounded w-full"
                            placeholder="Специализация"
                        />
                    </div>
                    <div className="flex gap-3">
                        <div className="flex-1 space-y-3">
                            <p className="text-xl">Цена за услугу, тг.</p>
                            <input
                                type="text"
                                className="p-2 border border-gray-100 focus:outline-none focus:border-pink-purple rounded w-full"
                                placeholder="Цена"
                            />
                        </div>
                        <div className="flex-1 space-y-3">
                            <p className="text-xl">Длительность, мин.</p>
                            <input
                                type="text"
                                className="p-2 border border-gray-100 focus:outline-none focus:border-pink-purple rounded w-full"
                                placeholder="Время"
                            />
                        </div>
                    </div>
                    <div className="space-y-2">
                        <p className="text-xl">Направление</p>
                        <div className="flex space-x-2 items-center">
                            <input
                                type="radio"
                                name="opt"
                                className="radio radio-primary"
                                value=""
                            />
                            <p>Нужно</p>
                        </div>
                        <div className="flex space-x-2 items-center">
                            <input
                                type="radio"
                                name="opt"
                                className="radio radio-primary"
                                value=""
                            />
                            <p>Не нужно</p>
                        </div>
                    </div>
                    <div className="space-y-3">
                        <p className="text-xl">Кто оказывает услугу</p>
                        <input
                            type="text"
                            className="p-2 border border-gray-100 focus:outline-none focus:border-pink-purple rounded w-full"
                            placeholder="Поиск по специальности"
                        />
                    </div>
                    <div className="flex justify-end">
                        <button className="bg-pink-purple p-3 rounded text-white">
                            Добавить услугу
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ServicesPage;
