import React from "react";
import styled from "styled-components";

const DoctorPage = () => {
    return (
        <div className="space-y-5">
            <div className="text-sm breadcrumbs text-base-300">
                <ul>
                    <li>
                        <a>Все сотрудники</a>
                    </li>
                    <li>
                        <a>Ахметолдинов Азамат Сандыбаевич</a>
                    </li>
                </ul>
            </div>
            <div>
                <p className="font-bold text-3xl">
                    Ахметолдинов Азамат Сандыбаевич
                </p>
            </div>
            <div className="grid grid-cols-12 gap-4">
                <div className="col-start-1 col-end-4 space-y-3">
                    <DoctorAvatarCard className="flex justify-start items-end p-5 bg-white rounded-xl">
                        <button className="bg-gray-100 text-pink-purple py-2 px-4 rounded">
                            Изменить фото
                        </button>
                    </DoctorAvatarCard>
                    <div className="flex">
                        <div className="flex-1">
                            <p className="text-gray-400">Статус:</p>
                            <p className="text-main-green">В клинике</p>
                        </div>
                        <div className="flex-1">
                            <p className="text-gray-400">Кабинет:</p>
                            <p>105</p>
                        </div>
                    </div>
                    <div className="flex flex-col justify-center space-y-2">
                        <button className="border border-pink-purple text-pink-purple w-full py-2 rounded hover:bg-pink-purple hover:text-white">
                            Вызвать к себе
                        </button>
                        <button className="text-pink-purple py-1 px-3 w-full hover:bg-pink-purple hover:text-white rounded">
                            Страница врача на странице
                        </button>
                    </div>
                </div>
                <div className="col-start-4 col-end-9 p-5 bg-white rounded-xl space-y-3">
                    <div className="flex justify-between">
                        <p className="text-xl">Информация</p>
                        <button className="text-pink-purple flex rounded py-1 px-3 hover:bg-pink-purple hover:text-white">
                            Изменить{" "}
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-6 w-6"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                />
                            </svg>
                        </button>
                    </div>
                    <hr />
                    <div>
                        <p className="font-bold">Профессиональная</p>
                        <div>
                            <p className="text-gray-400">Специализация</p>
                            <p>Хирург, уролог, андролог</p>
                        </div>
                        <div>
                            <p className="text-gray-400">Принимает</p>
                            <p>Детей и взрослых</p>
                        </div>
                        <div>
                            <p className="text-gray-400">Стаж работы</p>
                            <p>4 года стажа</p>
                        </div>
                        <div>
                            <p className="text-gray-400">
                                Процент от зароботка
                            </p>
                            <p>50% - клинике</p>
                        </div>
                    </div>
                    <div>
                        <p className="font-bold">Контактная</p>
                        <div>
                            <p className="text-gray-400">Телефон</p>
                            <p>+7 747 798 93 32</p>
                        </div>
                        <div>
                            <p className="text-gray-400">Почта</p>
                            <p>shvardik@mail.ru</p>
                        </div>
                    </div>
                    <div>
                        <p className="font-bold">Личная</p>
                        <div>
                            <p className="text-gray-400">Имя</p>
                            <p>Азамат</p>
                        </div>
                        <div>
                            <p className="text-gray-400">Фамилия</p>
                            <p>Ахметолдинов</p>
                        </div>
                        <div>
                            <p className="text-gray-400">Отчество</p>
                            <p>Сандыбаевич</p>
                        </div>
                    </div>
                </div>
                <div className="col-start-9 col-end-13 space-y-5">
                    <ScheduleCard className="p-5 rounded-xl space-y-3">
                        <div className="flex justify-between">
                            <p className="text-xl">Расписание</p>
                            <button className="text-pink-purple flex rounded py-1 px-3 hover:bg-pink-purple hover:text-white">
                                Изменить{" "}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                    />
                                </svg>
                            </button>
                        </div>
                        <hr />
                        <div className="flex">
                            <div className="flex-1">
                                <p>Понедельник</p>
                                <p>Вторник</p>
                                <p>Среда</p>
                                <p>Четверг</p>
                                <p>Пятцница</p>
                                <p>Суббота</p>
                                <p>Воскресенье</p>
                            </div>
                            <div className="flex flex-col flex-1 items-end">
                                <p>9:00 - 21:00</p>
                                <p>9:00 - 21:00</p>
                                <p>9:00 - 21:00</p>
                                <p>9:00 - 21:00</p>
                                <p>9:00 - 21:00</p>
                                <p>9:00 - 21:00</p>
                                <p>9:00 - 21:00</p>
                            </div>
                        </div>
                    </ScheduleCard>
                    <div className="bg-white rounded-xl p-5">
                        <div className="flex justify-between">
                            <p className="text-xl">Записи</p>
                            <button className="text-pink-purple flex rounded py-1 px-3 hover:bg-pink-purple hover:text-white">
                                Изменить{" "}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth={2}
                                        d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                                    />
                                </svg>
                            </button>
                        </div>
                        <div>
                            <p className="text-gray-400">
                                Понедельник, 27 сентября
                            </p>
                            <div>
                                <RecordCard />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

const RecordCard = () => {
    return (
        <div className="flex space-x-4 items-center">
            <p>9:00</p>
            <div className="h-full w-sm bg-pink-purple"></div>
            <div>
                <p>Иванов Иван</p>
                <p className="text-gray-400">
                    Первичная консультация андролога
                </p>
            </div>
        </div>
    );
};

const DoctorAvatarCard = styled.div`
    height: 300px;
    background-image: url("/icons/specialist.svg");
    background-position: center bottom;
    background-repeat: no-repeat;
    background-size: contain;
    z-index: 0;
`;

const ScheduleCard = styled.div`
    background-color: #f8f0ff;
`;

export default DoctorPage;
