import React, { useEffect } from "react";
import Link from "next/link";
import { ElevatedContainer } from "components/atoms/ElevatedContainer";
import TabsHead from "src/plugins/tab-routing/components/TabsHead";
import { useTabRouting } from "src/plugins/tab-routing/hooks/useTabRouting";
import Router from "next/router";

const StaffPage = () => {
    useEffect(() => {
        Router.push("staff?route=staff-doctors");
    }, []);
    const routes: TabRoute[] = [
        {
            slug: "staff-doctors",
            label: "Врачи",
            component: <DoctorsContainer />,
        },
        {
            slug: "staff-secretary",
            label: "Секретари",
            component: <SecretaryContainer />,
        },
        {
            slug: "staff-med",
            label: "Медперсонал",
            component: <MedPersonalContainer />,
        },
    ];
    const TabBody = useTabRouting({ routes });
    return (
        <div>
            <div className="flex justify-between">
                <p className="font-bold text-4xl">Сотрудники</p>
                <div className="flex space-x-2">
                    <button className="border border-pink-purple px-10 py-2 rounded text-pink-purple hover:bg-pink-purple hover:text-white">
                        Бывшие сотрудники
                    </button>
                    <Link href="/add-doctor">
                        <button className="flex bg-pink-purple p-2 text-white rounded items-center">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                                    clipRule="evenodd"
                                />
                            </svg>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                                    clipRule="evenodd"
                                />
                            </svg>
                        </button>
                    </Link>
                </div>
            </div>
            <div className="grid grid-cols-12 gap-3 my-5">
                <div className="col-start-1 col-end-6 bg-white p-1 rounded-xl">
                    <TabsHead routes={routes}></TabsHead>
                </div>
                <div className="col-start-6 col-end-9 bg-white rounded dropdown">
                    <div
                        tabindex="0"
                        className="flex items-center justify-between bg-white p-2 h-full rounded cursor-pointer"
                    >
                        Специализации
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </div>
                    <ul
                        tabIndex="0"
                        className="p-2 shadow menu dropdown-content bg-base-100 rounded-box w-52"
                    >
                        <li>
                            <a>Гинеколог</a>
                        </li>
                        <li>
                            <a>Уролог</a>
                        </li>
                        <li>
                            <a>Врач</a>
                        </li>
                    </ul>
                </div>
                <div className="col-start-9 col-end-13 bg-white rounded flex items-center p-3 space-x-3">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                        />
                    </svg>
                    <input
                        type="text"
                        placeholder="Поиск по имени"
                        className="px-2 focus:outline-none"
                    />
                </div>
            </div>
            <div>{TabBody}</div>
        </div>
    );
};

const DoctorsContainer = () => {
    return (
        <div className="grid grid-cols-3 gap-4 bg-white p-5 rounded-2xl">
            <DoctorCard />
            <DoctorCard />
            <DoctorCard />
            <DoctorCard />
            <DoctorCard />
            <DoctorCard />
            <DoctorCard />
            <DoctorCard />
        </div>
    );
};

const SecretaryContainer = () => {
    return (
        <div className="grid grid-cols-3 gap-4 bg-white p-5 rounded-2xl">
            <SecretaryCard />
            <SecretaryCard />
            <SecretaryCard />
            <SecretaryCard />
            <SecretaryCard />
            <SecretaryCard />
            <SecretaryCard />
            <SecretaryCard />
        </div>
    );
};

const MedPersonalContainer = () => {
    return (
        <div className="grid grid-cols-3 gap-4 bg-white p-5 rounded-2xl">
            <MedPersonalCard />
            <MedPersonalCard />
            <MedPersonalCard />
            <MedPersonalCard />
            <MedPersonalCard />
            <MedPersonalCard />
            <MedPersonalCard />
            <MedPersonalCard />
        </div>
    );
};

const DoctorCard = () => {
    return (
        <Link href="/staff/1/doctor">
            <ElevatedContainer className="rounded-lg">
                <div className="pt-5 px-5 space-y-2 cursor-pointer">
                    <p className="text-special-green">В клинике</p>
                    <p>Ахметолдинов Азамат Сандыбекович</p>
                    <p className="text-gray-400">Хирург, Уролог-андролог</p>
                    <p className="text-gray-400">101 кабинет</p>
                    <img
                        src="/icons/specialist.svg"
                        alt="Доктор"
                        className="self-end"
                    />
                </div>
            </ElevatedContainer>
        </Link>
    );
};

const SecretaryCard = () => {
    return (
        <Link href="/staff/1/doctor">
            <ElevatedContainer className="rounded-lg">
                <div className="py-5 px-5 space-y-2 cursor-pointer">
                    <p className="text-special-green">В клинике</p>
                    <p>Колоскова Анна Анатольевна</p>
                    <p className="text-gray-400">Тел: +7 707 231 26 32</p>
                </div>
            </ElevatedContainer>
        </Link>
    );
};

const MedPersonalCard = () => {
    return (
        <Link href="/staff/1/doctor">
            <ElevatedContainer className="rounded-lg">
                <div className="py-5 px-5 space-y-2 cursor-pointer">
                    <p className="text-special-green">В клинике</p>
                    <p>Колоскова Анна Анатольевна</p>
                    <p className="text-gray-400">Медбрат</p>
                    <p className="text-gray-400">105 кабинет</p>
                </div>
            </ElevatedContainer>
        </Link>
    );
};

export default StaffPage;
