import React, { useState, useEffect } from "react";
import MoneyBarChart from "components/atoms/MoneyBarChart";
import PatientsBarChart from "components/atoms/PatientsBarChart";
import ServiceBarChart from "components/atoms/ServiceBarChart";
import TabsHead from "src/plugins/tab-routing/components/TabsHead";
import Router, { useRouter } from "next/router";
import Moment from "moment";

import { GET_STATS } from "graphql/query";
import { useQuery } from "@apollo/client";

type TabProps = {
    slug: string;
    children: React.ReactElement;
};
const Tab = (props: TabProps) => {
    const router = useRouter();
    const currentPath = router.query["route"];
    if (currentPath === props.slug) {
        return <>{props.children}</>;
    }
    return <></>;
};

const StatisticsPage = () => {
    Moment.locale("en");
    const [stats, setStats] = useState({
        data: [],
        totalMoneyEarnt: 0,
        totalIndividualPatients: 0,
        totalSessionSum: 0,
        startDate: "",
        endDate: "",
    });

    const routes: TabRoute[] = [
        {
            slug: "money-chart",
            label: "Деньги",
            component: <></>,
        },
        {
            slug: "pateints-chart",
            label: "Пациенты",
            component: <></>,
        },
        {
            slug: "services-chart",
            label: "Услуги",
            component: <></>,
        },
    ];
    useEffect(() => {
        Router.push("statistics?route=money-chart");
    }, []);

    const { error } = useQuery(GET_STATS, {
        variables: {
            firstDate: Date.parse("2020-11-18T11:41:04.069Z"),
            secondDate: Date.parse("2021-12-18T11:41:04.069Z"),
            period: "Month",
        },
        onCompleted: (data) => {
            setStats(data.getGeneralStats);
        },
    });

    if (error) {
        return <div>{JSON.stringify(error)}</div>;
    }

    return (
        <div className="space-y-5">
            <div className="flex justify-between">
                <p className="text-4xl font-bold">Статистика</p>
                <div className="flex space-x-2">
                    <div className="flex p-1 rounded bg-white space-x-2">
                        <div className="px-2 py-1 hover:bg-pink-purple rounded cursor-pointer hover:text-white">
                            <p>Месяц</p>
                        </div>
                        <div className="px-2 py-1 hover:bg-pink-purple rounded cursor-pointer hover:text-white">
                            <p>Квартал</p>
                        </div>
                        <div className="px-2 py-1 hover:bg-pink-purple rounded cursor-pointer hover:text-white">
                            <p>Полгода</p>
                        </div>
                        <div className="px-2 py-1 hover:bg-pink-purple rounded cursor-pointer hover:text-white">
                            <p>Года</p>
                        </div>
                    </div>
                    <div className="flex text-gray-400 px-2 py-1 items-center bg-white rounded space-x-2">
                        <button>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-6 w-6"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                />
                            </svg>
                        </button>
                        <p>{`${Moment(stats.startDate).format(
                            "d.MM.YY",
                        )} - ${Moment(stats.endDate).format("d.MM.YY")}`}</p>
                    </div>
                </div>
            </div>
            <div className="bg-white p-5 rounded-2xl space-y-4">
                <div className="flex justify-between">
                    <div className="flex rounded">
                        <TabsHead routes={routes}></TabsHead>
                    </div>
                    <div></div>
                </div>
                <Tab slug="money-chart">
                    <MoneyBarChart stats={stats} />
                </Tab>
                <Tab slug="pateints-chart">
                    <PatientsBarChart stats={stats} />
                </Tab>
                <Tab slug="services-chart">
                    <ServiceBarChart stats={stats} />
                </Tab>
                <div className="grid grid-cols-3 gap-4">
                    <div className="bg-gray-100 rounded p-5">
                        <p className="text-4xl font-bold">
                            {stats.totalMoneyEarnt} ₸
                        </p>
                        <p className="text-gray-400">
                            заработано в заданный период
                        </p>
                    </div>
                    <div className="bg-gray-100 rounded p-5">
                        <p className="text-4xl font-bold">
                            {stats.totalIndividualPatients}
                        </p>
                        <p className="text-gray-400">
                            новых клиентов в заданный год
                        </p>
                    </div>
                    <div className="bg-gray-100 rounded p-5">
                        <p className="text-4xl font-bold">
                            {stats.totalSessionSum}
                        </p>
                        <p className="text-gray-400">
                            услуг оказано в заданный период
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StatisticsPage;
