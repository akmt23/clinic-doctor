import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

interface TabsHeadProps {
    routes: TabRoute[];
}

const TabsHeadForStaff: React.FC<TabsHeadProps> = ({ routes }) => {
    const router = useRouter();
    const currentPath = router.query.route;

    return (
        <div className="space-y-2">
            {routes.map((route) => (
                <Link
                    key={route.slug}
                    href={{
                        href: route.slug,
                        query: { ...router.query, route: route.slug },
                    }}
                    replace
                >
                    <div
                        className={`cursor-pointer ${
                            route.slug === currentPath
                                ? "text-black"
                                : "text-gray-400"
                        }`}
                    >
                        {route.label}
                    </div>
                </Link>
            ))}
        </div>
    );
};

export default TabsHeadForStaff;
