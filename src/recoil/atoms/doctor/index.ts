import { useRecoilState } from "recoil";
import { atom } from "recoil";
import { Atoms } from "@recoil/constants";

export const doctorAtom = atom({
    key: Atoms.Doctor,
    default: {},
});
