import { useRecoilState } from "recoil";

import { doctorAtom } from "@recoil/atoms";

export const useDoctor: () => [
    any,
    { setCurrentDoctor: (doctor: any) => void },
] = () => {
    const [doctor, setDoctor] = useRecoilState(doctorAtom);

    const setCurrentDoctor = (doctor: any) => {
        setDoctor(doctor);
    };
    return [doctor, { setCurrentDoctor }];
};
