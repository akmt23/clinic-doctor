import {
    ApolloClient,
    createHttpLink,
    InMemoryCache,
    NormalizedCacheObject,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

// let apolloClient: ApolloClient<NormalizedCacheObject> | null = null;

const httpLink = createHttpLink({
    uri: "http://94.247.128.224:3000/graphql",
});

const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem("JWT");
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `${token}` : "",
        },
    };
});

// const createApolloClient = new ApolloClient({
//     ssrMode: typeof window === "undefined",
//     //uri: "https://api.spacex.land/graphql/",
//     uri: "http://94.247.128.224:3000/graphql",
//     cache: new InMemoryCache(),
// });

export const initializeApollo = () => {
    const client = new ApolloClient({
        link: authLink.concat(httpLink),
        cache: new InMemoryCache(),
    });
    return client;
    // For SSG and SSR always create a new Apollo Client
    // if (typeof window === "undefined") {
    //     return createApolloClient;
    // }

    // // Create the Apollo Client once in the client
    // if (!apolloClient) {
    //     apolloClient = createApolloClient;
    // }

    // return apolloClient;
};
